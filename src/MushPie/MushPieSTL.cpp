//%Header {
/*****************************************************************************
 *
 * File: src/MushPie/MushPieSTL.cpp
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } RHiVVZfysx7GhK866I95jw
/*
 * $Id: MushPieSTL.cpp,v 1.4 2007/04/18 09:22:59 southa Exp $
 * $Log: MushPieSTL.cpp,v $
 * Revision 1.4  2007/04/18 09:22:59  southa
 * Header and level fixes
 *
 * Revision 1.3  2006/06/01 15:39:37  southa
 * DrawArray verification and fixes
 *
 * Revision 1.2  2005/05/19 13:02:13  southa
 * Mac release work
 *
 * Revision 1.1  2004/01/02 11:57:47  southa
 * Created
 *
 */

#include "MushPieSTL.h"

