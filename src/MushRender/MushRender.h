//%includeGuardStart {
#ifndef MUSHRENDER_H
#define MUSHRENDER_H
//%includeGuardStart } 5fmF16UOHKnng34sEDWRmg
//%Header {
/*****************************************************************************
 *
 * File: src/MushRender/MushRender.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } CrJ9VKEajiPshxSI8chQxA
/*
 * $Id: MushRender.h,v 1.7 2007/04/18 09:23:00 southa Exp $
 * $Log: MushRender.h,v $
 * Revision 1.7  2007/04/18 09:23:00  southa
 * Header and level fixes
 *
 * Revision 1.6  2006/09/07 16:38:52  southa
 * Vertex shader
 *
 * Revision 1.5  2006/06/01 15:39:38  southa
 * DrawArray verification and fixes
 *
 * Revision 1.4  2005/08/29 18:40:57  southa
 * Solid rendering work
 *
 * Revision 1.3  2005/07/16 14:22:59  southa
 * Added diagnostic renderer
 *
 * Revision 1.2  2005/07/04 11:10:43  southa
 * Rendering pipeline
 *
 * Revision 1.1  2005/07/01 10:36:46  southa
 * MushRender work
 *
 */

#include "MushRenderMesh.h"
#include "MushRenderMeshDiagnostic.h"
#include "MushRenderMeshShader.h"
#include "MushRenderMeshSolid.h"
#include "MushRenderMeshWireframe.h"
#include "MushRenderSpec.h"
#include "MushRenderStandard.h"

//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
