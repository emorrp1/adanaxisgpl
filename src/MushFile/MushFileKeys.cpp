//%Header {
/*****************************************************************************
 *
 * File: src/MushFile/MushFileKeys.cpp
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } 1lRxLeOHDjZx4vZkV/Y1fQ
/*
 * $Id: MushFileKeys.cpp,v 1.3 2007/04/18 09:22:32 southa Exp $
 * $Log: MushFileKeys.cpp,v $
 * Revision 1.3  2007/04/18 09:22:32  southa
 * Header and level fixes
 *
 * Revision 1.2  2006/12/16 10:57:23  southa
 * Encrypted files
 *
 * Revision 1.1  2006/12/15 14:03:28  southa
 * File key handling
 *
 */

#include "MushFileKeys.h"

MUSHCORE_SINGLETON_INSTANCE(MushFileKeys);

using namespace Mushware;
using namespace std;

MushFileKeys::MushFileKeys()
{
    m_keyEntries[0] = NULL; // Add the empty key
}

bool
MushFileKeys::Lookup(const Mushware::U8 *& outpData, Mushware::U32 inID)
{
    bool retVal = false;
    
    std::map<Mushware::U32, Mushware::U8 *>::const_iterator p = m_keyEntries.find(inID);
    if (p != m_keyEntries.end())
    {
        outpData = p->second;
        retVal = true;
    }
    
    return retVal;
}

bool
MushFileKeys::Exists(Mushware::U32 inID)
{
    return m_keyEntries.find(inID) != m_keyEntries.end();
}

void
MushFileKeys::KeyEntryAdd(Mushware::U32 inID, Mushware::U8 *inpData)
{
    if (m_keyEntries.find(inID) != m_keyEntries.end())
    {
        MushcoreLog::Sgl().WarningLog() << "Replacing file key " << inID << endl;
    }
    m_keyEntries[inID] = inpData;
}
