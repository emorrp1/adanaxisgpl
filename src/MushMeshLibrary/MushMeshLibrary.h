//%includeGuardStart {
#ifndef MUSHMESHLIBRARY_H
#define MUSHMESHLIBRARY_H
//%includeGuardStart } 8hSXwlwf47Uh9dCvKfKknA
//%Header {
/*****************************************************************************
 *
 * File: src/MushMeshLibrary/MushMeshLibrary.h
 *
 * Copyright: Andy Southgate 2005-2007
 *
 * This file may be used and distributed under the terms of the Mushware
 * Software Licence version 1.4, under the terms for 'Proprietary original
 * source files'.  If not supplied with this software, a copy of the licence
 * can be obtained from Mushware Limited via http://www.mushware.com/.
 * One of your options under that licence is to use and distribute this file
 * under the terms of the GNU General Public Licence version 2.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } 1tJUfqL7V5PnWUw8CpCoCg
/*
 * $Id: MushMeshLibrary.h,v 1.11 2007/06/27 12:58:28 southa Exp $
 * $Log: MushMeshLibrary.h,v $
 * Revision 1.11  2007/06/27 12:58:28  southa
 * Debian packaging
 *
 * Revision 1.10  2007/04/18 09:22:53  southa
 * Header and level fixes
 *
 * Revision 1.9  2006/09/12 15:28:50  southa
 * World sphere
 *
 * Revision 1.8  2006/08/01 17:21:33  southa
 * River demo
 *
 * Revision 1.7  2006/07/17 14:43:40  southa
 * Billboarded deco objects
 *
 * Revision 1.6  2006/06/16 01:02:32  southa
 * Ruby mesh generation
 *
 * Revision 1.5  2006/06/14 18:45:48  southa
 * Ruby mesh generation
 *
 * Revision 1.4  2006/06/14 11:20:08  southa
 * Ruby mesh generation
 *
 * Revision 1.3  2006/06/01 15:39:33  southa
 * DrawArray verification and fixes
 *
 * Revision 1.2  2005/07/14 12:50:31  southa
 * Extrusion work
 *
 * Revision 1.1  2005/07/12 20:39:05  southa
 * Mesh library work
 *
 */

#include "MushMeshLibraryBase.h"
#include "MushMeshLibraryExtruder.h"
#include "MushMeshLibraryExtrusionContext.h"
#include "MushMeshLibraryFGenExtrude.h"
#include "MushMeshLibraryMaker.h"
#include "MushMeshLibraryPrism.h"
#include "MushMeshLibrarySingleFacet.h"
#include "MushMeshLibraryStandard.h"
#include "MushMeshLibraryVGenExtrude.h"
#include "MushMeshLibraryWorldSphere.h"

//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
