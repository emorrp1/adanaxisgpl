//%Header {
/*****************************************************************************
 *
 * File: src/MushSkin/MushSkin.cpp
 *
 * Copyright: Andy Southgate 2005-2007
 *
 * This file may be used and distributed under the terms of the Mushware
 * Software Licence version 1.4, under the terms for 'Proprietary original
 * source files'.  If not supplied with this software, a copy of the licence
 * can be obtained from Mushware Limited via http://www.mushware.com/.
 * One of your options under that licence is to use and distribute this file
 * under the terms of the GNU General Public Licence version 2.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } cypo0a2fv6fbzSxEkLlYcg
/*
 * $Id: MushSkin.cpp,v 1.5 2007/06/27 12:58:31 southa Exp $
 * $Log: MushSkin.cpp,v $
 * Revision 1.5  2007/06/27 12:58:31  southa
 * Debian packaging
 *
 * Revision 1.4  2007/04/18 09:23:04  southa
 * Header and level fixes
 *
 * Revision 1.3  2006/08/01 17:21:37  southa
 * River demo
 *
 * Revision 1.2  2006/06/01 15:39:40  southa
 * DrawArray verification and fixes
 *
 * Revision 1.1  2005/08/29 18:40:57  southa
 * Solid rendering work
 *
 */

#include "MushSkin.h"
