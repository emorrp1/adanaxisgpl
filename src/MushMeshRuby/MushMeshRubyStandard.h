//%includeGuardStart {
#ifndef MUSHMESHRUBYSTANDARD_H
#define MUSHMESHRUBYSTANDARD_H
//%includeGuardStart } sxR91hM3031V/dSSOaL+ww
//%Header {
/*****************************************************************************
 *
 * File: src/MushMeshRuby/MushMeshRubyStandard.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } k53DxewXC8hL/kADI9hOmQ
/*
 * $Id: MushMeshRubyStandard.h,v 1.3 2007/04/18 09:22:56 southa Exp $
 * $Log: MushMeshRubyStandard.h,v $
 * Revision 1.3  2007/04/18 09:22:56  southa
 * Header and level fixes
 *
 * Revision 1.2  2006/06/12 16:01:23  southa
 * Ruby mesh generation
 *
 * Revision 1.1  2006/06/12 11:59:39  southa
 * Ruby wrapper for MushMeshVector
 *
 */

#include "API/mushMushcore.h"
#include "API/mushMushMesh.h"
#include "API/mushMushMeshLibrary.h"
#include "API/mushMushRuby.h"

//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
