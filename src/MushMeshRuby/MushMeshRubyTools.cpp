//%Header {
/*****************************************************************************
 *
 * File: src/MushMeshRuby/MushMeshRubyTools.cpp
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } 3VYstokBMbVTW6GI4B/N4Q
/*
 * $Id: MushMeshRubyTools.cpp,v 1.6 2007/03/26 16:31:36 southa Exp $
 * $Log: MushMeshRubyTools.cpp,v $
 * Revision 1.6  2007/03/26 16:31:36  southa
 * L2 work
 *
 * Revision 1.5  2006/08/20 14:19:22  southa
 * Seek operation
 *
 * Revision 1.4  2006/07/18 16:58:38  southa
 * Texture fixes
 *
 * Revision 1.3  2006/07/17 14:43:41  southa
 * Billboarded deco objects
 *
 * Revision 1.2  2006/06/20 19:06:54  southa
 * Object creation
 *
 * Revision 1.1  2006/06/13 19:30:38  southa
 * Ruby mesh generation
 *
 */

#include "MushMeshRubyTools.h"

#include "MushMeshRubyPost.h"
#include "MushMeshRubyRotation.h"
#include "MushMeshRubyVector.h"

using namespace Mushware;
using namespace std;

MUSHRUBYOBJ_INSTANCE(MushMeshTools);

Mushware::tRubyValue
MushMeshRubyTools::RotationInAxis(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0,
								  Mushware::tRubyValue inArg1)
{
	MushRubyValue param0(inArg0);
	MushRubyValue param1(inArg1);
	
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
	
	MushMeshRubyRotation::WRef(retVal) = MushMeshTools::QuaternionRotateInAxis(param0.U32(), param1.Val());
	
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::RotationInXYPlane(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
	MushMeshRubyRotation::WRef(retVal) = MushMeshTools::QuaternionRotateInAxis(MushMeshTools::kAxisXY, MushRubyValue(inArg0).Val());
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::RotationInZWPlane(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
	MushMeshRubyRotation::WRef(retVal) = MushMeshTools::QuaternionRotateInAxis(MushMeshTools::kAxisZW, MushRubyValue(inArg0).Val());
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::RotationInXZPlane(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
	MushMeshRubyRotation::WRef(retVal) = MushMeshTools::QuaternionRotateInAxis(MushMeshTools::kAxisXZ, MushRubyValue(inArg0).Val());
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::RotationInYWPlane(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
	MushMeshRubyRotation::WRef(retVal) = MushMeshTools::QuaternionRotateInAxis(MushMeshTools::kAxisYW, MushRubyValue(inArg0).Val());
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::RotationInXWPlane(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
	MushMeshRubyRotation::WRef(retVal) = MushMeshTools::QuaternionRotateInAxis(MushMeshTools::kAxisXW, MushRubyValue(inArg0).Val());
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::RotationInYZPlane(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
	MushMeshRubyRotation::WRef(retVal) = MushMeshTools::QuaternionRotateInAxis(MushMeshTools::kAxisYZ, MushRubyValue(inArg0).Val());
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::RandomOrientation(Mushware::tRubyValue inSelf)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
	MushMeshRubyRotation::WRef(retVal) = MushMeshTools::RandomOrientation();
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::RandomUnitVector(Mushware::tRubyValue inSelf)
{
	Mushware::tRubyValue retVal = MushMeshRubyVector::NewInstance();
	MushMeshRubyVector::WRef(retVal) = MushMeshTools::RandomUnitVector();
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::RandomAngularVelocity(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
    MushMeshTools::RandomAngularVelocity(MushMeshRubyRotation::WRef(retVal), MushRubyValue(inArg0).Val());
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::RandomSeedSet(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0)
{
	srand(MushRubyValue(inArg0).U32());
	return kRubyQnil;;
}

Mushware::tRubyValue
MushMeshRubyTools::SeekRotation(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0,
                                Mushware::tRubyValue inArg1)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
    MushMeshTools::PartialRotateToWAxis(MushMeshRubyRotation::WRef(retVal),
                                        MushMeshRubyVector::Ref(inArg0),
                                        MushRubyValue(inArg1).Val());
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::TurnToFace(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0,
                              Mushware::tRubyValue inArg1, Mushware::tRubyValue inArg2)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
    MushMeshTools::TurnToFace(MushMeshRubyRotation::WRef(retVal),
                                        MushMeshRubyPost::Ref(inArg0),
                                        MushMeshRubyVector::Ref(inArg1),
                                        MushRubyValue(inArg2).Val());
	return retVal;
}

Mushware::tRubyValue
MushMeshRubyTools::Slerp(Mushware::tRubyValue inSelf, Mushware::tRubyValue inArg0,
                         Mushware::tRubyValue inArg1, Mushware::tRubyValue inArg2)
{
	Mushware::tRubyValue retVal = MushMeshRubyRotation::NewInstance();
    MushMeshRubyRotation::WRef(retVal) =
        MushMeshOps::SlerpNormalised(MushMeshRubyRotation::Ref(inArg0),
                                     MushMeshRubyRotation::Ref(inArg1),
                                     MushRubyValue(inArg2).Val());
	return retVal;
}

void
MushMeshRubyTools::RubyInstall(void)
{
	ObjInstall("MushTools");

	MushRubyUtil::SingletonMethodDefineTwoParams(ObjKlass(), "cRotationInPlane", RotationInAxis);
	MushRubyUtil::SingletonMethodDefineOneParam(ObjKlass(), "cRotationInXYPlane", RotationInXYPlane);
	MushRubyUtil::SingletonMethodDefineOneParam(ObjKlass(), "cRotationInZWPlane", RotationInZWPlane);
	MushRubyUtil::SingletonMethodDefineOneParam(ObjKlass(), "cRotationInXZPlane", RotationInXZPlane);
	MushRubyUtil::SingletonMethodDefineOneParam(ObjKlass(), "cRotationInYWPlane", RotationInYWPlane);
	MushRubyUtil::SingletonMethodDefineOneParam(ObjKlass(), "cRotationInXWPlane", RotationInXWPlane);
	MushRubyUtil::SingletonMethodDefineOneParam(ObjKlass(), "cRotationInYZPlane", RotationInYZPlane);
	MushRubyUtil::SingletonMethodDefineNoParams(ObjKlass(), "cRandomOrientation", RandomOrientation);
	MushRubyUtil::SingletonMethodDefineNoParams(ObjKlass(), "cRandomUnitVector", RandomUnitVector);
	MushRubyUtil::SingletonMethodDefineOneParam(ObjKlass(), "cRandomAngularVelocity", RandomAngularVelocity);
	MushRubyUtil::SingletonMethodDefineOneParam(ObjKlass(), "cRandomSeedSet", RandomSeedSet);
	MushRubyUtil::SingletonMethodDefineTwoParams(ObjKlass(), "cSeekRotation", SeekRotation);
	MushRubyUtil::SingletonMethodDefineThreeParams(ObjKlass(), "cTurnToFace", TurnToFace);
	MushRubyUtil::SingletonMethodDefineThreeParams(ObjKlass(), "cSlerp", Slerp);
	//MushRubyUtil::SingletonMethodDefineThreeParams(ObjKlass(), "cTurnToFace", TurnToFace);
}

MUSHRUBY_INSTALL(MushMeshRubyTools);
