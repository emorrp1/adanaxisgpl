//%includeGuardStart {
#ifndef MUSHMUSHRENDER_H
#define MUSHMUSHRENDER_H
//%includeGuardStart } OacmsbxJZoKMF6juP7YJ5Q
//%Header {
/*****************************************************************************
 *
 * File: src/API/mushMushRender.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } xtzjkcXpxQ0BWsME29CFPg
/*
 * $Id: mushMushRender.h,v 1.5 2007/04/18 09:21:58 southa Exp $
 * $Log: mushMushRender.h,v $
 * Revision 1.5  2007/04/18 09:21:58  southa
 * Header and level fixes
 *
 * Revision 1.4  2006/06/22 19:07:27  southa
 * Build fixes
 *
 * Revision 1.3  2006/06/01 15:38:45  southa
 * DrawArray verification and fixes
 *
 * Revision 1.2  2005/07/12 20:39:04  southa
 * Mesh library work
 *
 */

#if defined(HAVE_CONFIG_H)  && !defined(MUSHWARE_CONFIG_H)
#define MUSHWARE_CONFIG_H 1
#include "config.h"
#endif

#if defined(HAVE_MUSHRENDER_MUSHRENDER_H)
#include <MushRender/MushRender.h>
#elif defined(HAVE_MUSHRENDER_H)
#include <MushRender.h>
#else
#include "MushRender/MushRender.h"
#endif

//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
