//%includeGuardStart {
#ifndef MUSHRUBYSTL_H
#define MUSHRUBYSTL_H
//%includeGuardStart } RBDOgxaBlY/2QIxOB0n3Sw
//%Header {
/*****************************************************************************
 *
 * File: src/MushRuby/MushRubySTL.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } bDTkSe+LvLfzywChRcEeXw
/*
 * $Id: MushRubySTL.h,v 1.3 2007/04/18 09:23:03 southa Exp $
 * $Log: MushRubySTL.h,v $
 * Revision 1.3  2007/04/18 09:23:03  southa
 * Header and level fixes
 *
 * Revision 1.2  2006/06/22 19:07:35  southa
 * Build fixes
 *
 * Revision 1.1  2006/04/20 00:22:45  southa
 * Added ruby executive
 *
 */

#if defined(HAVE_CONFIG_H)  && !defined(MUSHWARE_CONFIG_H)
#define MUSHWARE_CONFIG_H 1
#include "config.h"
#endif

#if defined(HAVE_MUSHCORE_MUSHCORE_H)
#include <Mushcore/MushcoreSTL.h>
#elif defined(HAVE_MUSHCORE_H)
#include <MushcoreSTL.h>
#else
#include "Mushcore/MushcoreSTL.h"
#endif
//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
