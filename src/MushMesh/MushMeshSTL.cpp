//%Header {
/*****************************************************************************
 *
 * File: src/MushMesh/MushMeshSTL.cpp
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } defh/+yT8VDhNg6xWR8blw
/*
 * $Id: MushMeshSTL.cpp,v 1.5 2007/04/18 09:22:50 southa Exp $
 * $Log: MushMeshSTL.cpp,v $
 * Revision 1.5  2007/04/18 09:22:50  southa
 * Header and level fixes
 *
 * Revision 1.4  2006/06/01 15:39:32  southa
 * DrawArray verification and fixes
 *
 * Revision 1.3  2005/05/19 13:02:11  southa
 * Mac release work
 *
 * Revision 1.2  2004/01/02 21:13:11  southa
 * Source conditioning
 *
 * Revision 1.1  2003/10/14 10:46:05  southa
 * MeshMover creation
 *
 */

#include "MushMeshSTL.h"
