//%includeGuardStart {
#ifndef ADANAXISPIECEWORLD_H
#define ADANAXISPIECEWORLD_H
//%includeGuardStart } yMM+emyFXLFbQRCK187Htg
//%Header {
/*****************************************************************************
 *
 * File: src/Adanaxis/AdanaxisPieceWorld.h
 *
 * Copyright: Andy Southgate 2005-2007
 *
 * This file may be used and distributed under the terms of the Mushware
 * Software Licence version 1.4, under the terms for 'Proprietary original
 * source files'.  If not supplied with this software, a copy of the licence
 * can be obtained from Mushware Limited via http://www.mushware.com/.
 * One of your options under that licence is to use and distribute this file
 * under the terms of the GNU General Public Licence version 2.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } 6H1SseU4jjQvAsXctj9opQ
/*
 * $Id: AdanaxisPieceWorld.h,v 1.5 2007/06/27 12:58:24 southa Exp $
 * $Log: AdanaxisPieceWorld.h,v $
 * Revision 1.5  2007/06/27 12:58:24  southa
 * Debian packaging
 *
 * Revision 1.4  2007/04/18 09:22:02  southa
 * Header and level fixes
 *
 * Revision 1.3  2006/10/30 17:03:51  southa
 * Remnants creation
 *
 * Revision 1.2  2006/08/01 17:21:26  southa
 * River demo
 *
 * Revision 1.1  2006/07/19 10:22:15  southa
 * World objects
 *
 */

#include "AdanaxisStandard.h"

#include "AdanaxisPiece.h"

#include "API/mushMushGL.h"
#include "API/mushMushMesh.h"
#include "API/mushMushRender.h"

//:xml1base MushGamePiece
//:generate virtual standard ostream xml1
class AdanaxisPieceWorld : public MushGamePiece
{
public:
    explicit AdanaxisPieceWorld(const std::string& inID = "");
    
    virtual void Render(MushGameLogic& ioLogic, MushRenderMesh& inRender, const MushGameCamera& inCamera);
private:
        
//%classPrototypes {
public:
    virtual const char *AutoName(void) const;
    virtual MushcoreVirtualObject *AutoClone(void) const;
    virtual MushcoreVirtualObject *AutoCreate(void) const;
    static MushcoreVirtualObject *AutoVirtualFactory(void);
    virtual void AutoPrint(std::ostream& ioOut) const;
    virtual bool AutoXMLDataProcess(MushcoreXMLIStream& ioIn, const std::string& inTagStr);
    virtual void AutoXMLPrint(MushcoreXMLOStream& ioOut) const;
//%classPrototypes } 1oBgFruy5qHAaudtV+Hcmg
};
//%inlineHeader {
inline std::ostream&
operator<<(std::ostream& ioOut, const AdanaxisPieceWorld& inObj)
{
    inObj.AutoPrint(ioOut);
    return ioOut;
}
//%inlineHeader } +6D5ssvr6WCcjMqQ4Y2w3g
//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
