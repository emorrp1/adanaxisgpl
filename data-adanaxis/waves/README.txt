#%Header {
##############################################################################
#
# File data-adanaxis/waves/README.txt
#
# Author Andy Southgate 2006-2007
#
# This file contains original work by Andy Southgate.  The author and his
# employer (Mushware Limited) irrevocably waive all of their copyright rights
# vested in this particular version of this file to the furthest extent
# permitted.  The author and Mushware Limited also irrevocably waive any and
# all of their intellectual property rights arising from said file and its
# creation that would otherwise restrict the rights of any party to use and/or
# distribute the use of, the techniques and methods used herein.  A written
# waiver can be obtained via http://www.mushware.com/.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################
#%Header } 7vq09p7XN5+SY1/m34zSSQ

Sample sources for GPL release are:

gpl-explo0.wav   Golgotha old_generic.mp3
gpl-explo1.wav   Golgotha supertank_lvl_one_main_barrel_interrior_44khz.mp3
gpl-explo2.wav   Golgotha shockwave.mp3
gpl-explo3.wav   Golgotha supertank_lvl_one_main_barrel_interrior_44khz.mp3
gpl-explo4.wav   Golgotha big_begrtha_gun_fire_22khz.mp3
gpl-explo5.wav   Golgotha generic.mp3
gpl-explo6.wav   Golgotha acid.mp3
gpl-explo7.wav   Golgotha super_mortar.mp3 (an extract from)

gpl-fire0.wav    Mushware
gpl-fire1.wav    Golgotha old_auto1_lp.wav
gpl-fire2.wav    Golgotha main1.mp3 (truncated)
gpl-fire3.wav    Golgotha old_auto1_lp.wav (processed)
gpl-fire4.wav    Golgotha fire_one_22khz.mp3
gpl-fire5.wav    Golgotha old_peon_tank2.mp3
gpl-fire6.wav    Golgotha acid (2).mp3
gpl-fire7.wav    Golgotha acid2.mp3
gpl-fire8.wav    Golgotha electric_tower_firing_three_22khz_lp.wav
gpl-fire9.wav    Golgotha supertank_fires_rocket_22khz.mp3

gpl-load0.wav    Golgotha main_barrel_refuel.mp3
gpl-load1.wav    Golgotha main_barrel_refuel.mp3
gpl-load2.wav    Golgotha main_barrel_refuel.mp3
gpl-load3.wav    Golgotha main_barrel_refuel.mp3
gpl-load4.wav    Golgotha electric_car_charged_lp.wav
gpl-load5.wav    Golgotha main_barrel_refuel.mp3 + main_missile_refuel.mp3
gpl-load6.wav    Golgotha missile_truck_lower.mp3
gpl-load7.wav    Golgotha missile_truck_raise.mp3
gpl-load8.wav    Golgotha missile_truck_lower.mp3
gpl-load9.wav    Golgotha rising_missile_bay.mp3 + missile_truck_lp.wav
