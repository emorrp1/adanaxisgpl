#%Header {
##############################################################################
#
# File data-adanaxis/ruby/AdanaxisShaderLibrary.rb
#
# Copyright Andy Southgate 2006-2007
#
# This file may be used and distributed under the terms of the Mushware
# Software Licence version 1.4, under the terms for 'Proprietary original
# source files'.  If not supplied with this software, a copy of the licence
# can be obtained from Mushware Limited via http://www.mushware.com/.
# One of your options under that licence is to use and distribute this file
# under the terms of the GNU General Public Licence version 2.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################
#%Header } 8sKQgC2wZFWHg65tXgKo2Q
# $Id: AdanaxisShaderLibrary.rb,v 1.8 2007/06/27 12:58:12 southa Exp $
# $Log: AdanaxisShaderLibrary.rb,v $
# Revision 1.8  2007/06/27 12:58:12  southa
# Debian packaging
#
# Revision 1.7  2007/04/18 09:21:53  southa
# Header and level fixes
#
# Revision 1.6  2007/04/16 08:41:06  southa
# Level and header mods
#
# Revision 1.5  2007/03/13 21:45:08  southa
# Release process
#
# Revision 1.4  2006/09/10 10:30:51  southa
# Shader billboarding
#
# Revision 1.3  2006/09/09 15:59:27  southa
# Shader colour calculations
#
# Revision 1.2  2006/09/07 16:38:49  southa
# Vertex shader
#
# Revision 1.1  2006/09/07 10:02:36  southa
# Shader interface
#

class AdanaxisShaderLibrary < MushShaderLibrary
  def self.cCreate
    super
  end
end
