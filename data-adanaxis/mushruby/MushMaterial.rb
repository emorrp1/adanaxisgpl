#%Header {
##############################################################################
#
# File data-adanaxis/mushruby/MushMaterial.rb
#
# Author Andy Southgate 2006-2007
#
# This file contains original work by Andy Southgate.  The author and his
# employer (Mushware Limited) irrevocably waive all of their copyright rights
# vested in this particular version of this file to the furthest extent
# permitted.  The author and Mushware Limited also irrevocably waive any and
# all of their intellectual property rights arising from said file and its
# creation that would otherwise restrict the rights of any party to use and/or
# distribute the use of, the techniques and methods used herein.  A written
# waiver can be obtained via http://www.mushware.com/.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################
#%Header } mT6SwZO9t74RwGwAcfmVaw
# $Id: MushMaterial.rb,v 1.4 2007/03/13 21:45:03 southa Exp $
# $Log: MushMaterial.rb,v $
# Revision 1.4  2007/03/13 21:45:03  southa
# Release process
#
# Revision 1.3  2006/08/01 17:21:13  southa
# River demo
#
# Revision 1.2  2006/08/01 13:41:07  southa
# Pre-release updates
#

class MushMaterial
# Class: MushMaterial
#
# Description:
#
# This object describes the material used to render a mesh.  Typically it
# just names the texture to be used.
#
# Example:
#
# (example)
# MushGLTexture.cDefine(
#    :name          => 'texture1',
#    :type          => 'TIFF',
#    :filename      => MushConfig.GlobalPixelsPath+'/texturefile1.tiff'
#	)
# MushMaterial.cDefine(
#     :name => 'material1',
#     :texture_name => 'texture1'
# )
#  ...
# mesh.mAddMaterial('material1')
# (end)
#
# Method: cDefine
#
# Defines a material, adding it to the global database.  It can then be used
# by adding it to a <MushMesh> object.
#
# Parameters:
#
# name - Name used to refer to this material
# texture_name - name of the texture used by this material
#
# Method: to_xml
#
# Returns XML for the entire material database.  This is a singeton method.
#
# (example)
# puts MushMaterial.to_xml
# (end)
#
# Parameters:
#
# None.
#
# Returns:
#
# A string containing the XML.
#
# Group: Links
#- Wrapper and implementation file:doxygen/class_mush_g_l_material.html

  def self.cDefine(params)
    cRubyDefine(params)
  end
  
  def self.to_xml
    cRubyto_xml()
  end	
end

